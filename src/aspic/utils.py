from .snk import *
from .tokens import sdot, fdot

def has_succeeded(N, M=None):
    """ Test if a Control-Flow Petri net has succeeded.
    :param N: the Petri net
    :param M: the Petri net marking. If None, the current marking is retreived.
    :return: true if a success token is marking the exit place
    """
    m = N.get_marking() if M is None else M
    x = N.status(exit)[0]
    return sdot in m(x)

def has_failed(N, M=None):
    """ Test if a Control-Flow Petri net has failed.
    :param N: the Petri net
    :param M: the Petri net marking. If None, the current marking is retreived.
    :return: true if an exception token is marking the exit place
    """
    m = N.get_marking() if M is None else M
    x = N.status(exit)[0]
    return fdot in m(x)

def initialize(N, init_marking={}):
    """ Initialize the marking of a CFPN.
    Puts a 'sdot' in the CFPN entry place.
    :param N: a CFPN
    :type N: PetriNet
    :param init_marking: defines the initial marking of each buffer
    :type init_marking: a dict of place names and marking
    :return: the CFPN with its initial marking
    :rtype: PetriNet
    """
    e = N.status(entry)[0]
    N.place(e).add(sdot)
    for place, marking in init_marking.items():
        p = N.status(buffer(place))[0]
        N.place(p).reset(marking)
    # Hack: add a label to all places
    for p in N.place():
        p.label()

def draw(pn, name, fired=[], place_attr=None, trans_attr=None):
    """ Draws a Petri net to a file using graphviz.

    Entry places are drawn with a double circle.
    Exit places are drawn with a triple circle.
    Control-flow places (entry, exit, internals) are filled in light gray.
    Places marked by a control-flow token are drawn in blue
    Buffer places are drawn with top and bottom lines.

    :param pn: the Petri net
    :param name: file name
    :type name: str
    :param fired: list of fired transitions; these transitions are drawn in red
    :type fired: list of str
    """
    def draw_place(p, attr):
        if place_attr:
            place_attr(p, attr)

        try:
            if p.has_label('skill'):
                attr['label'] = p.label('skill')
        except Exception as ex:
            print("Error in label of place {}: {}".format(p, ex))

        if sdot in p or fdot in p:
            attr['color'] = 'blue'
            attr['fillcolor'] = 'azure2'
        elif p.status in [entry, exit, internal]:
            attr['fillcolor'] = 'lightgray'
        else:
            attr['fillcolor'] = 'white'

        if p.status in [entry]:
            attr['peripheries'] = 2
        elif p.status in [exit]:
            attr['peripheries'] = 3

        if (type(p.status) == Buffer):
            attr['label'] = '%s\n%s' % (p.status, p.tokens)
            attr['shape'] = 'oval'
            attr['style'] = 'diagonals'

    def draw_transition (t, attr) :
        if trans_attr:
            trans_attr(t, attr)

        if str(t.guard) == 'True' :
            attr['label'] = t.name
        else :
            attr['label'] = '%s\n%s' % (t.name, t.guard)
        if t in fired:
            attr['color'] = 'red'
            attr['fillcolor'] = 'darksalmon'
        else:
            attr['fillcolor'] = 'white'

    def draw_graph(pn, attr):
        attr['bgcolor'] = "transparent"
        #attr['pad'] = 0.5
        #attr['nodesep'] = 1
        #attr['ranksep'] = 2

    try:
        return pn.draw(name, engine='dot', graph_attr=draw_graph,
            place_attr=draw_place, trans_attr=draw_transition)
    except Exception as e:
        print(e)
        return None

def step(N, once=False, verbose=False):
    """ Makes a step on the Petri net.
    Computes the firable transitions, and fires the first one.

    :param N: a CFPN
    :type N: PetriNet
    :param once: if true, only one transition is fired; otherwise, test all transitions of the CFPN
    :type once: boolean
    :param verbose: if true, displays firable transition and the resulting marking
    :type verbose: boolean
    :return: true if a transition has been fired, false if no transition is firable
    :rtype: boolean
    """
    ret = False
    for t in N.transition():
        m = t.modes()
        if len(m) > 0:
            if verbose: print("firing {} with {}".format(t, m[0]))
            t.fire(m[0])
            if verbose: print("M = {}".format(N.get_marking()))
            ret = True
            if once: break
    return ret
