#####
# Copyright 2017 ONERA
#
# This file is part of the ASPiC project.
#
# This project is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3 as
# published by the Free Software Foundation.
#
# This project is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this project.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
#####
from .snk import *

class RosClientTransition(Transition):
    """ A specific implementation of the transition in case of a Ros client

    When a transition is tested to compute the set of firable transitions,
    output arcs are evaluated to check token types. In case of the Ros client,
    this would result in the client connecting to the server, even if the
    transition is finally not fired. This implementation of _check then
    removes type checking on output arcs.
    """
    def _check(self, binding, tokens, input):
        if not self.guard(binding) :
            return False
        if tokens :
            for place, label in self.input() :
                if not (label.flow(binding) <= place.tokens) :
                    return False
        if input :
            for place, label in self.input() :
                try :
                    place.check(v.value for v in iterate(label.bind(binding)))
                except ValueError :
                    return False
        return True
