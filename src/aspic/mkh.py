import functools, warnings
import snakes.hashables as sh

def _hashable (obj) :
    "convert most objects to a hashable equivalent"
    if hasattr(obj, "__hash__") and callable(obj.__hash__) :
        # object is already hashable
        return obj
    elif isinstance(obj, dict) :
        # dict -> hdict
        return sh.hdict((k, _hashable(v)) for k, v in obj.items())
    elif isinstance(obj, list) :
        # list -> hlist
        return sh.hlist(_hashable(i) for i in obj)
    elif isinstance(obj, set) :
        # set -> hset
        return sh.hset(_hashable(i) for i in obj)
    else :
        # this is an arbitray instance, it should have a __hash__ method,
        # hash(obj) may fallback to return id(obj) which is usually not a good
        # solution since it's often inconsistent with equality
        warnings.warn("instance of %s is not hashable" % obj.__class__)
        return obj

def hret (fun) :
    """Decorator to ensure that a function returns a hashable object"""
    @functools.wraps(fun)
    def wrapper (*l, **h) :
        return _hashable(fun(*l, **h))
    return wrapper
