#####
# Copyright 2017 ONERA
#
# This file is part of the ASPiC project.
#
# This project is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3 as
# published by the Free Software Foundation.
#
# This project is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this project.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
#####
from .snk import Token, Instance, Status

from enum import Enum, unique

@unique
class SkillStatus(Enum):
    """ Enumeration representing the execution status of a skill

    Possible values are:
    * pending: the skill execution is to be started
    * active: the skill is under execution
    * stopped: the skill has successfully stopped
    * failed: the skill has failed
    """
    pending = 0
    active = 1
    stopped = 2
    failed = 3

class ControlFlowToken (Token) :
    """ Control flow token type

    ASPiC provides the following control-flow tokens:
    * 'sdot': the success (black) token
    * 'fdot': the exception (white) token
    """
    def __init__ (self, value=None) :
        Token.__init__(self, value)
    def __str__(self):
        return self.value + "dot"
    def __repr__(self):
        return self.value + "dot"
    def __or__(self, other):
        if self.value == 's':
            return sdot
        elif other.value == 's':
            return sdot
        else:
            return fdot

tControlFlowToken = Instance(ControlFlowToken)
sdot = ControlFlowToken('s')
fdot = ControlFlowToken('f')

private = Status('private')
