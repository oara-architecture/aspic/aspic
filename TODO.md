## TODO

### Brainstorming

* ingénierie des exigences (achieve, maintain, ...)
* everything is a skill? (fonctions internes du superviseur, style appel à des fonctions de planif, conversions?)
* où se place le monitoring dans l'archi ? idée :
  * Monitoring des skills : dans l'archi MAUVE ; fait des retours au superviseur sous forme de Detect skill
  * Monitoring du plan : en // du superviseur ; fait des retours au GoalManager
* supervision du plan, au sens STN par exemple : où ?
* Skill MAUVE symmétrique avec un "Stop" (sym. Launch)

### PN Refs

* _Recovery Nets: Towards Self-Adaptive Workflow Systems_: recovery procedures to adapt to failures (skip current task, ...)
* _Modeling Workflow with Petri Nets_
* _Petri net based resource modeling and analysis of workflows with task failures_

https://link.springer.com/chapter/10.1007%2F978-3-662-45730-6_1
https://link.springer.com/chapter/10.1007/978-3-540-87405-8_18

## Deps

* snakes
* enum34
