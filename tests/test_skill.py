#####
# Copyright 2017 ONERA
#
# This file is part of the ASPiC project.
#
# This project is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License version 3 as
# published by the Free Software Foundation.
#
# This project is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this project.  If not, see <https://www.gnu.org/licenses/lgpl-3.0>.
#####
from aspic import *
from aspic.utils import draw

A = SkillPetriNet("A")
draw(A, "skill.pdf")

goto = SkillPetriNet("goto", inputs=["target"], locks={"motors": 1})
draw(goto, "goto_skill.pdf")

obs = SkillPetriNet("obs", outputs=["pose"])
draw(obs, "obs_skill.pdf")

h = DefaultHandler("h")
draw(h, "handler.pdf")
goto_h = handle(goto, h)
draw(goto_h, "skill_w_handler.pdf")
