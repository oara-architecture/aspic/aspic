#!/usr/bin/env python

from distutils.core import setup

setup(
    name = 'aspic',
    version = '1.0.0',
    description = 'An Acting system based on Skill Petri net Composition (ASPiC)',
    url = 'https://gitlab.com/charles-lesire/aspic',
    author = 'Charles Lesire-Cabaniols',
    author_email = 'charles.lesire@onera.fr',
    packages = ['aspic'],
    package_dir={'': 'src'}
)
